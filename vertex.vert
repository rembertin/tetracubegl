attribute vec3 vertex_position;
attribute vec2 a_texcoord;
uniform mat4 PMatrix;
uniform mat4 VMatrix;
uniform mat4 MMatrix;
varying vec2 v_texcoord;

void main() {
    gl_Position = PMatrix * VMatrix * MMatrix * vec4(vertex_position,1.0);
    v_texcoord = a_texcoord;
}