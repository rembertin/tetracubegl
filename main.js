function loadText(url) {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', url, false);
    xhr.overrideMimeType("text/plain");
    xhr.send(null);
    if(xhr.status === 200)
        return xhr.responseText;
    else {
        return null;
    }
}

var gl;
var program;
var attribPos;
var bufferCube;
var bufferTetraedre;
var bufferSol;
var tx=0, ty=0;
var uView, uPerspective, uModel, uSampler;
var projMatrix = mat4.create();
var viewMatrix = mat4.create();
var attribTexture;
var textureCube, textureTetra, textureSol;
var bufferTextureCube, bufferTextureTetra, bufferTextureSol;
var xCamera = 8, yCamera = 5, zCamera = 18;
var xLook = 0, yLook = 0, zLook = 0;
var zoom = 1;

/*** INITIALISATION OF THE SCENE ***/

function initShader(string_src, shaderType, program_dest) {
    var shaderSrc = loadText(string_src);

    var shader = gl.createShader(shaderType);
    gl.shaderSource(shader, shaderSrc);
    gl.compileShader(shader);

    if(!gl.getShaderParameter(shader, gl.COMPILE_STATUS))
        console.log("Erreur lors de la compilation du shader '"+ string_src +"':\n"+gl.getShaderInfoLog(shader));

    gl.attachShader(program_dest, shader);
}

function initShaders() {
    program = gl.createProgram();
    initShader('vertex.vert', gl.VERTEX_SHADER, program);
    initShader('fragment.frag', gl.FRAGMENT_SHADER, program);

    gl.linkProgram(program);
    if(!gl.getProgramParameter(program, gl.LINK_STATUS))
        console.log("Erreur lors du linkage du programme :\n"+gl.getProgramInfoLog(program));
    gl.useProgram(program);
}

function initShadersVariables() {
	attribPos = gl.getAttribLocation(program, "vertex_position");
    attribTexture = gl.getAttribLocation(program, "a_texcoord");
    uPerspective = gl.getUniformLocation(program, 'PMatrix');
    uModel = gl.getUniformLocation(program, "MMatrix");
    uView = gl.getUniformLocation(program, "VMatrix");
    uSampler = gl.getUniformLocation(program, "sampler");
}

function createObject(array_coordinates, int_bufferVertexSize) {
    var bufferObject = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, bufferObject);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(array_coordinates), gl.STATIC_DRAW);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    bufferObject.vertexSize = int_bufferVertexSize;
    bufferObject.numVertices = array_coordinates.length/int_bufferVertexSize;
    return bufferObject;
}

function createTexture(string_src) {
    var texture = gl.createTexture();
    var image = new Image();
    image.onload = function() {
        gl.bindTexture(gl.TEXTURE_2D, texture);
        gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
        gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, image);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.REPEAT);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.REPEAT);
        gl.bindTexture(gl.TEXTURE_2D, null);
    };
    image.src = string_src;
    return texture;
}

function createBufferTexture(array_coordinates) {
    var bufferTexture = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, bufferTexture);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(array_coordinates), gl.STATIC_DRAW);
    bufferTexture.vertexSize = 2;
    bufferTexture.numVertices = array_coordinates.length/bufferTexture.vertexSize;
    gl.bindBuffer(gl.ARRAY_BUFFER, null);
    return bufferTexture;
}

function initCube() {
    var coordsCube = [
        //face avant
        -1,2,1,  -1,0,1,  1,2,1,  1,2,1,  -1,0,1,  1,0,1,
        //face arriere
        -1,2,-1,  -1,0,-1,  1,2,-1,  1,2,-1,  -1,0,-1,  1,0,-1,
        //face gauche
        -1,2,-1,  -1,0,-1,  -1,2,1,  -1,2,1,  -1,0,-1,  -1,0,1,
        //face droite
        1,2,-1,  1,0,-1,  1,2,1,  1,2,1,  1,0,-1,  1,0,1,
        //face haut
        -1,2,-1,  -1,2,1,  1,2,-1,  1,2,-1,  -1,2,1,  1,2,1,
        //face bas
        -1,0,-1,  -1,0,1,  1,0,-1,  1,0,-1,  -1,0,1,  1,0,1
    ];

    bufferCube = createObject(coordsCube, 3);

    textureCube = createTexture("wood03.jpg");

    var coordsTextureCube = [
        //face avant
        0.0, 1.0,   0.0, 0.0,   1.0, 1.0,   1.0, 1.0,   0.0, 0.0,   1.0, 0.0,
        //face arrière
        0.0, 1.0,   0.0, 0.0,   1.0, 1.0,   1.0, 1.0,   0.0, 0.0,   1.0, 0.0,
        //face gauche
        0.0, 1.0,   0.0, 0.0,   1.0, 1.0,   1.0, 1.0,   0.0, 0.0,   1.0, 0.0,
        //face droite
        0.0, 1.0,   0.0, 0.0,   1.0, 1.0,   1.0, 1.0,   0.0, 0.0,   1.0, 0.0,
        //face haut
        0.0, 1.0,   0.0, 0.0,   1.0, 1.0,   1.0, 1.0,   0.0, 0.0,   1.0, 0.0,
        //face bas
        0.0, 1.0,   0.0, 0.0,   1.0, 1.0,   1.0, 1.0,   0.0, 0.0,   1.0, 0.0
    ];
    bufferTextureCube = createBufferTexture(coordsTextureCube);
}

function initTetra(){
    var coordsTetra = [ //tetrahedron of edge length 2
        0,Math.sqrt(2/3)*2,0,
        -1,0,Math.sqrt(3)/2,
        1,0,Math.sqrt(3)/2,
        0,0,-Math.sqrt(3)/2,
        0,Math.sqrt(2/3)*2,0,
        -1,0,Math.sqrt(3)/2
    ];
    bufferTetraedre = createObject(coordsTetra, 3);
    
    textureTetra = createTexture("metal12.jpg");

    var coordsTextureTetra = [
        0.5, 1.0,
        0.0, 0.0,
        1.0, 0.0,
        0.5, 1.0,
        0.5, 1.0,
        0.0, 0.0
    ];
    bufferTextureTetra = createBufferTexture(coordsTextureTetra);
}

function initFloor(){
    var coordsFloor = [
        -15,0,-15,
        15,0,-15,
        15,0,15,
        -15,0,15,
        -15,0,-15
    ];

    /*var coordsTextureFloor = [
        0,1,
        1,1,
        1,0,
        0,0,
        0,1
    ];*/
    
    var coordsTextureFloor = [];
    var coordsGrid = [];
    var GRID_SIZE = 100;
    var step = 1.8/GRID_SIZE;
    for (var i= 0 ; i<GRID_SIZE ; i++) {
        for (var j=0 ; j<GRID_SIZE ; j++) {
            var x = i*step - 0.9;
            var y = j*step - 0.9;
            var currentQuad = [x,y,
                                x+step,y,
                                x+step,y+step,
                                x,y,
                                x+step,y+step,
                                x,y+step];
            coordsGrid = coordsGrid.concat(currentQuad);
            coordsTextureFloor = coordsTextureFloor.concat(coordsFloor);
        }
    }
    bufferSol = createObject(coordsFloor, 3);

    textureSol = createTexture("tile11.jpg");

    bufferTextureSol = createBufferTexture(coordsTextureFloor);
}

/*** DRAWING OF THE SCENE ***/

function drawObjectWithTexture(buffer_object, int_glDrawMethod, texture, buffer_texture) {
    gl.activeTexture(gl.TEXTURE0);
    gl.bindTexture(gl.TEXTURE_2D, texture);
    gl.uniform1i(uSampler, 0);

    gl.bindBuffer(gl.ARRAY_BUFFER, buffer_object);
    gl.vertexAttribPointer(attribPos, buffer_object.vertexSize, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(attribPos);

    gl.bindBuffer(gl.ARRAY_BUFFER, buffer_texture);
    gl.vertexAttribPointer(attribTexture, buffer_texture.vertexSize, gl.FLOAT, false,0,0);
    gl.enableVertexAttribArray(attribTexture);

    gl.drawArrays(int_glDrawMethod, 0, buffer_object.numVertices);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);
    gl.bindTexture(gl.TEXTURE_2D, null);
}

function drawCube() {
    var modelMatrix = mat4.create();
    mat4.identity(modelMatrix);
    mat4.rotateY(modelMatrix, modelMatrix, time);
    mat4.translate(modelMatrix, modelMatrix, [3,0,-4]);
    mat4.scale(modelMatrix, modelMatrix, [0.5,0.5,0.5]);
    gl.uniformMatrix4fv(uModel, false, modelMatrix);
	drawObjectWithTexture(bufferCube, gl.TRIANGLES, textureCube, bufferTextureCube);
}

function drawTetrahedron(){
    var modelMatrix = mat4.create();
    mat4.identity(modelMatrix);
    mat4.translate(modelMatrix, modelMatrix, [-2,1,3]);
    mat4.scale(modelMatrix, modelMatrix, [0.5,0.5,0.5]);
    mat4.rotateY(modelMatrix, modelMatrix, time);
    gl.uniformMatrix4fv(uModel, false, modelMatrix);
    drawObjectWithTexture(bufferTetraedre, gl.TRIANGLE_STRIP, textureTetra, bufferTextureTetra);
}

function drawFloor(){
    var modelMatrix = mat4.create();
    mat4.identity(modelMatrix);
    mat4.scale(modelMatrix, modelMatrix, [0.5,0.5,0.5]);
    gl.uniformMatrix4fv(uModel, false, modelMatrix);
    drawObjectWithTexture(bufferSol, gl.TRIANGLE_STRIP, textureSol, bufferTextureSol);
}

var time=0;

function draw() {
    requestAnimationFrame(draw);

    gl.enable(gl.DEPTH_TEST);
    gl.clearColor(0.0,0.0,1.0,0.5);
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    mat4.perspective(projMatrix, Math.PI/4*zoom, 1, 0.1, 100);
    mat4.lookAt(viewMatrix, [xCamera,yCamera,zCamera], [xLook,yLook,zLook], [0,1,0]);
    gl.uniformMatrix4fv(uPerspective,false, projMatrix);
    gl.uniformMatrix4fv(uView,false, viewMatrix);

    drawCube();
    drawTetrahedron();
    drawFloor();

    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    time = (time + 0.01)% (2*Math.PI);
}

function main() {
    var canvas = document.getElementById('dawin-webgl');
    gl = canvas.getContext('webgl');
    
    if (!gl) {
        console.log('ERREUR : Echec du chargement du contexte !');
        return;
    }

    initShaders();
    initShadersVariables();
    initCube();
    initTetra();
    initFloor();

    draw();
}

KEY_DOWN    = 40;
KEY_UP      = 38;
KEY_LEFT    = 37;
KEY_RIGHT   = 39;
KEY_PLUS = 107;
KEY_MINUS = 109;
KEY_R = 82;
KEY_L = 76;
KEY_SPACE = 32;

var increment = 0.1;
document.onkeydown = function(e) {
	e.preventDefault();
    switch(e.keyCode){
        case KEY_DOWN:
        if (e.ctrlKey) {
        	zCamera += increment;
        	zLook += increment;
        }
        else {
        	yCamera += -increment;
        	yLook += -increment;
        }
        
        break;
        case KEY_UP:
        if (e.ctrlKey) {
        	zCamera -= increment;
        	zLook -= increment;
        }
        else {
        	yCamera += increment;
        	yLook += increment;
        }
        
        break;
        case KEY_LEFT:
        xCamera -= increment;
        xLook -= increment;
        break;
        case KEY_RIGHT:
        xCamera += increment;
        xLook += increment;
        break;
        case KEY_PLUS:
        zoom -= increment;
        break;
        case KEY_MINUS:
        zoom += increment;
        break;
    }
};

var mouseDown = 0;
document.onmousedown = function(e) {
    xMouse = e.clientX; yMouse = e.clientY;
    mouseDown = true;
}
document.onmouseup = function() {
  mouseDown = false;
}

canvas = document.getElementById('dawin-webgl');
canvas.addEventListener("mousemove", function(e) {
    var incrementEye = 0.1;
    if (mouseDown) {
        xLook -= incrementEye*(e.clientX - xMouse);
        xMouse = e.clientX;
        yLook += incrementEye*(e.clientY - yMouse);
        yMouse = e.clientY;
    }
});