precision mediump float;
varying vec2 v_texcoord;
uniform sampler2D sampler;

void main() {
    gl_FragColor = texture2D(sampler, v_texcoord);
}
